document.addEventListener("DOMContentLoaded", function () {
  const signUpButton = document.getElementById("signUp");
  const signInButton = document.getElementById("signIn");
  const container = document.getElementById("container");
  const signUp = document.getElementById("SignUp");
  const signIn = document.getElementById("SignIn");
  const notificationCard = document.getElementById("notificationCard");
  const notificationText = document.getElementById("notificationText");
  const closeButton = document.getElementById("closeButton");
  const toggleSignIn= document.getElementById("toggleSignIn");  
  const toggleSignUp= document.getElementById("toggleSignUp");  

  toggleSignUp.addEventListener('click', ()=>{
    container.classList.add("right-panel-active");
  });
  toggleSignIn.addEventListener('click', ()=>{
    container.classList.remove("right-panel-active");
  });

  signUpButton.addEventListener("click", () => {
    container.classList.add("right-panel-active");
  });
  signInButton.addEventListener("click", () => {
    container.classList.remove("right-panel-active");
  });

  closeButton.addEventListener("click", () => {
    notificationCard.classList.remove("show");
  });

  //SignUp
  signUp.addEventListener("click", function () {
    const username = document.getElementById("usernameSignUp").value;
    const email = document.getElementById("emailSignUp").value;
    const pass = document.getElementById("passwordSignUp").value;

    const usernameError = document.getElementById("usernameError");
    const emailError = document.getElementById("emailError");

    usernameError.textContent = "";
    emailError.textContent = "";

    if (!username || !email || !pass) {
      showNotification("Please fill in all the fields", "error");
      return;
    }

    let isUsernameExists = false;
    let isEmailExists = false;

    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      if (key === username) {
        isUsernameExists = true;
        break;
      }

      const data = JSON.parse(localStorage.getItem(key));
      if (data.email === email) {
        isEmailExists = true;
        break;
      }
    }

    if (isUsernameExists) {
      showNotification("Username already exists", "error");
    }

    if (isEmailExists) {
      showNotification("Email already exists", "error");
    }

    if (!isUsernameExists && !isEmailExists) {
      localStorage.setItem(username, JSON.stringify({ username, email, pass }));
      console.log("User signed up:", { username, email, pass });
      document.getElementById("usernameSignUp").value = "";
      document.getElementById("emailSignUp").value = "";
      document.getElementById("passwordSignUp").value = "";

      showNotification("Signed Up Successfully!", "success");
    }
  });
  //SignIn
  signIn.addEventListener("click", function () {
    console.log("Sign In button clicked!");
    const username = document.getElementById("usernameSignIn").value;
    const password = document.getElementById("passwordSignIn").value;

    if (!username || !password) {
      showNotification("Please fill in all the fields", "error");
      return;
    }

    let isLoggedIn = false;
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      const data = JSON.parse(localStorage.getItem(key));
      if (data.username === username && data.pass === password) {
        isLoggedIn = true;
        break;
      }
    }

    if (isLoggedIn) {
      console.log("User logged in:", { username });
      document.getElementById("usernameSignIn").value = "";
      document.getElementById("passwordSignIn").value = "";

      showNotification("Logged In Successfully!", "success");
      
      console.log('redirecting')
      setTimeout(() =>{
        window.location.replace("Dashboard.html?username=" + encodeURIComponent(username));
      },1000);
      console.log('redirecting success')
    } else {
      showNotification("Invalid username or password", "error");
    }
  });

  function showNotification(message, type) {
    notificationText.textContent = message;
    notificationCard.classList.add(type === "success" ? "success" : "error");
    notificationCard.classList.add("show");

    setTimeout(() => {
      notificationCard.classList.remove("show");
    }, 3000);
  }
});
