document.addEventListener("DOMContentLoaded", function () {
  const params = new URLSearchParams(window.location.search);
  const username = params.get("username");
  let loggedInUsername = username || null;
  console.log("Logged-in Username:", loggedInUsername);
  const allTasksContainer = document.querySelector(".AllTasks");
  const completedTasksContainer = document.querySelector(".CompletedTasks");

  //load tasks
  loadTasks();

  function loadTasks() {
    if (!loggedInUsername) return;

    const tasksKey = `tasks_${loggedInUsername}`;
    const tasks = JSON.parse(localStorage.getItem(tasksKey)) || [];

    allTasksContainer.innerHTML = "";
    completedTasksContainer.innerHTML = "";

    const completedTasksKey = `completed_tasks_${loggedInUsername}`;
    const completedTasks =
      JSON.parse(localStorage.getItem(completedTasksKey)) || [];

    tasks.forEach((task) => {
      if (!task.completed) {
        appendTaskToAllTasks(task);
      }
    });
    completedTasks.forEach((task) => {
      appendTaskToCompletedTasks(task);
    });
    allTasksContainer.style.display = tasks.length === 0 ? "none" : "block";
    completedTasksContainer.style.display =
      completedTasks.length === 0 ? "none" : "block";

    setupDeleteButtons();
  }

  function setupDeleteButtons() {
    const deleteButtons = document.querySelectorAll(".button-3");
    deleteButtons.forEach((button) => {
      button.addEventListener("click", () => {
        // Find the task associated with the delete button
        const taskDiv = button.parentElement.parentElement;
        // Check if the task is completed before removing it
        const isCompletedTask =
          taskDiv.parentElement === completedTasksContainer;
        if (!loggedInUsername || isCompletedTask) return;

        const titleHeading = taskDiv.querySelector("h3");
        const descriptionParagraph = taskDiv.querySelector("p");
        const title = titleHeading.textContent.replace("Title: ", "");
        const description = descriptionParagraph.textContent.replace(
          "Description: ",
          ""
        );
        const taskIdentifier = `${title}_${description}`;

        // Remove the task element from the DOM
        taskDiv.remove();
        const tasksKey = `tasks_${loggedInUsername}`;
        let tasks = JSON.parse(localStorage.getItem(tasksKey)) || [];
        tasks = tasks.filter((task) => {
          const taskIdentifierToCheck = `${task.title}_${task.description}`;
          return taskIdentifierToCheck !== taskIdentifier;
        });
        localStorage.setItem(tasksKey, JSON.stringify(tasks));
        const completedTasksKey = `completed_tasks_${loggedInUsername}`;
        let completedTasks =
          JSON.parse(localStorage.getItem(completedTasksKey)) || [];
        completedTasks = completedTasks.filter((task) => {
          const taskIdentifierToCheck = `${task.title}_${task.description}`;
          return taskIdentifierToCheck !== taskIdentifier;
        });
        localStorage.setItem(completedTasksKey, JSON.stringify(completedTasks));
      });
    });
  }

  const h1Tag = document.querySelector(".tag h1");
  h1Tag.textContent = `Hello, ${username || "Guest"}`;
  //Add Task
  const add = document.getElementById("add");
  add.addEventListener("click", (event) => {
    event.preventDefault(); // Prevent form submission (page reload)
    if (!loggedInUsername) {
      showNotification("You need to be logged in to add a task.", "error");
      return;
    }
    const titleInput = document.getElementById("title");
    const descriptionInput = document.getElementById("description");
    const title = titleInput.value;
    const description = descriptionInput.value;

    if (!title || !description) {
      showNotification(
        "Please enter a title and description for the task.",
        "error"
      );
      return;
    }

    const task = {
      title: title,
      description: description,
      completed: false,
    };

    const tasksKey = `tasks_${loggedInUsername}`;
    let tasks = JSON.parse(localStorage.getItem(tasksKey)) || [];
    tasks.push(task);
    localStorage.setItem(tasksKey, JSON.stringify(tasks));

    titleInput.value = "";
    descriptionInput.value = "";

    showNotification("Task added successfully!", "success");
    if (tasks.length === 0) {
      allTasksContainer.style.display = "none";
    } else {
      allTasksContainer.style.display = "block";
    }

   
    appendTaskToAllTasks(task);
    setupDeleteButtons();
  });

  function appendTaskToAllTasks(task) {
    const allTasksContainer = document.querySelector(".AllTasks");
    task.completed = false;

    const taskDiv = document.createElement("div");
    taskDiv.classList.add("item");

    const titleHeading = document.createElement("h3");
    titleHeading.textContent = `Title: ${task.title}`;

    const descriptionParagraph = document.createElement("p");
    descriptionParagraph.textContent = `Description: ${task.description}`;

    const buttonsDiv = document.createElement("div");
    buttonsDiv.classList.add("buttons");

    const editButton = document.createElement("button");
    editButton.classList.add("button-2");
    editButton.textContent = "Edit";

    const deleteButton = document.createElement("button");
    deleteButton.classList.add("button-3");
    deleteButton.textContent = "Delete";

    const completeButton = document.createElement("button");
    completeButton.classList.add("button-4");
    completeButton.textContent = "Complete";

    // Append the elements to the taskDiv
    buttonsDiv.appendChild(editButton);
    buttonsDiv.appendChild(deleteButton);
    buttonsDiv.appendChild(completeButton);
    taskDiv.appendChild(titleHeading);
    taskDiv.appendChild(descriptionParagraph);
    taskDiv.appendChild(buttonsDiv);

    allTasksContainer.appendChild(taskDiv);

    // Complete button event
    completeButton.addEventListener("click", () => {
      moveTaskToCompleted(taskDiv, task, buttonsDiv);
    });

    // Edit button event
    function setupEditForm() {
      const editForm = document.createElement("form");
      editForm.classList.add("edit-form");

      const editTitleInput = document.createElement("input");
      editTitleInput.type = "text";
      editTitleInput.value = task.title;
      editTitleInput.classList.add("edit-title");

      const editDescriptionInput = document.createElement("textarea");
      editDescriptionInput.value = task.description;
      editDescriptionInput.classList.add("edit-description");

      const saveButton = document.createElement("button");
      saveButton.type = "button";
      saveButton.classList.add("save-button");
      saveButton.textContent = "Save";

      editForm.appendChild(editTitleInput);
      editForm.appendChild(editDescriptionInput);
      editForm.appendChild(saveButton);
      taskDiv.appendChild(editForm);

      titleHeading.style.display = "none";
      descriptionParagraph.style.display = "none";
      buttonsDiv.style.display = "none";
      editForm.style.display = "block";

      saveButton.addEventListener("click", () => {
        const newTitle = editTitleInput.value;
        const newDescription = editDescriptionInput.value;

        if (!newTitle || !newDescription) {
          showNotification("Please enter a title and description.", "error");
          return;
        }

        const updatedTask = {
          title: newTitle,
          description: newDescription,
          completed: task.completed,
        };

        const tasksKey = `tasks_${loggedInUsername}`;
        let tasks = JSON.parse(localStorage.getItem(tasksKey)) || [];
        const index = tasks.findIndex(
          (t) => t.title === task.title && t.description === task.description
        );
        if (index !== -1) {
          tasks[index] = updatedTask;
          localStorage.setItem(tasksKey, JSON.stringify(tasks));
        }

        titleHeading.textContent = `Title: ${newTitle}`;
        descriptionParagraph.textContent = `Description: ${newDescription}`;

        titleHeading.style.display = "block";
        descriptionParagraph.style.display = "block";
        buttonsDiv.style.display = "flex";
        editForm.style.display = "none";

        showNotification("Task updated successfully!", "success");
      });
    }

    editButton.addEventListener("click", () => {
      const existingEditForm = taskDiv.querySelector(".edit-form");
      if (existingEditForm) {
        existingEditForm.remove();
      }
      setupEditForm();
    });

    // Delete button event
    deleteButton.addEventListener("click", () => {
      const taskDiv = deleteButton.parentElement.parentElement;
      const isCompletedTask = taskDiv.parentElement === completedTasksContainer;
      if (!loggedInUsername || isCompletedTask) return;

      const title = task.title;
      const description = task.description;
      const taskIdentifier = `${title}_${description}`;

      taskDiv.remove();
      const tasksKey = `tasks_${loggedInUsername}`;
      let tasks = JSON.parse(localStorage.getItem(tasksKey)) || [];
      tasks = tasks.filter((task) => {
        const taskIdentifierToCheck = `${task.title}_${task.description}`;
        return taskIdentifierToCheck !== taskIdentifier;
      });
      localStorage.setItem(tasksKey, JSON.stringify(tasks));
      const completedTasksKey = `completed_tasks_${loggedInUsername}`;
      let completedTasks =
        JSON.parse(localStorage.getItem(completedTasksKey)) || [];
      completedTasks = completedTasks.filter((task) => {
        const taskIdentifierToCheck = `${task.title}_${task.description}`;
        return taskIdentifierToCheck !== taskIdentifier;
      });
      localStorage.setItem(completedTasksKey, JSON.stringify(completedTasks));
    });
  }

  function moveTaskToCompleted(taskDiv, task, buttonsDiv) {
    const allTasksContainer = document.querySelector(".AllTasks");
    const completedTasksContainer = document.querySelector(".CompletedTasks");

    task.completed = true; 
    if (!loggedInUsername) return;
    const tasksKey = `tasks_${loggedInUsername}`;
    let tasks = JSON.parse(localStorage.getItem(tasksKey)) || [];
    const index = tasks.findIndex(
      (t) => t.title === task.title && t.description === task.description
    );

    if (index !== -1) {
      tasks[index].completed = true; 
      localStorage.setItem(tasksKey, JSON.stringify(tasks));
    }

    allTasksContainer.removeChild(taskDiv);
    buttonsDiv.style.display = "none";
    completedTasksContainer.appendChild(taskDiv);

    if (allTasksContainer.children.length === 0) {
      allTasksContainer.style.display = "none";
    }
    completedTasksContainer.style.display = "block";
    const completedTasksKey = `completed_tasks_${loggedInUsername}`;
    let completedTasks =
      JSON.parse(localStorage.getItem(completedTasksKey)) || [];
    completedTasks.push(task);
    localStorage.setItem(completedTasksKey, JSON.stringify(completedTasks));
  }

  function appendTaskToCompletedTasks(task) {
    const completedTasksContainer = document.querySelector(".CompletedTasks");
    const taskDiv = document.createElement("div");
    taskDiv.classList.add("item");

    const titleHeading = document.createElement("h3");
    titleHeading.textContent = `Title: ${task.title}`;

    const descriptionParagraph = document.createElement("p");
    descriptionParagraph.textContent = `Description: ${task.description}`;

    taskDiv.appendChild(titleHeading);
    taskDiv.appendChild(descriptionParagraph);
    completedTasksContainer.appendChild(taskDiv);
  }

  function handleLogout() {
    showNotification("Logged Out");
    setTimeout(() => {
      window.location.href = "index.html";
    }, 1000);
  }

  // Add an event listener to the "Logout" button

  console.log("logout called");
  const logoutButton = document.getElementById("logout");

  if (logoutButton) {
    logoutButton.addEventListener("click", handleLogout);
  }

  function showNotification(message, type) {
    notificationText.textContent = message;
    notificationCard.classList.add(type === "success" ? "success" : "error");
    notificationCard.classList.add("show");

    setTimeout(() => {
      notificationCard.classList.remove("show");
    }, 3000);
  }
});
